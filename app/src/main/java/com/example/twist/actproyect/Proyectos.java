package com.example.twist.actproyect;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Proyectos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyectos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

//this block stack a menu in an activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_proyectos,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent iLogReg = new Intent(getApplicationContext(), Login.class);
        int id = item.getItemId();
        switch (id) {
            case R.id.isesion:
                /*iBeneficiario.putExtra("arrBeneficiaios", beneficiarioses);
                iBeneficiario.putParcelableArrayListExtra("arrSubsidios", subsidios);
                startActivityForResult(iBeneficiario,31);*/
                startActivity(iLogReg);
                return true;
            case R.id.registrar:
                //Intent aReg = ;
                startActivity(new Intent(getApplicationContext(),Registro.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void accionBotonOnClick(View view){
        Intent proyects = new Intent(Proyectos.this,detalles.class);
        startActivity(proyects);
    }


}
